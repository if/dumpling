;;;; dumpling.asd

(asdf:defsystem #:dumpling
  :description "a very experimental blockchain"
  :author "---"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (#:ironclad
               #:cl-json
               #:hunchentoot
               #:drakma
               #:bordeaux-threads
               #:lparallel)
  :components ((:file "package")
               (:file "dumpling")))
