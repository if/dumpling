;;;; dumpling.lisp

(in-package #:dumpling)


;;;;;;;;;;;;;;;; concurrency stuff

(defparameter *kernel* (lparallel:make-kernel 1 :name "kernel"))
(defparameter *jobs* (lparallel.queue:make-queue))
;; (lparallel:end-kernel :wait t)

(defun flush-jobs (jobs)
  "remove all jobs from the queue, without executing them"
  (loop while (not (lparallel.queue:queue-empty-p jobs))
              do (lparallel.queue:pop-queue jobs)))

(defun make-job (f &rest args)
  "make job that calls `f' with `args'"
  (list (cons :function f) (cons :args args)))

(defun add-job (job jobs)
  "add a job to a job queue"
  (lparallel.queue:push-queue jobs job))

(defun do-next-job (jobs)
  "pop a job from the queue and execute it"
  (exec-job (lparallel.queue:pop-queue jobs)))

(defun exec-job (job)
  "run a job"
  (let ((f (cdr (assoc :function job)))
        (args (cdr (assoc :args job))))
    (apply f args)))



;;;;;;;;;;;;;;;; the meat and potatoes dumplings

(defclass <block> ()
  ((index :accessor index :initarg :index)
   (timestamp :accessor timestamp :initarg :timestamp)
   (prev-hash :accessor prev-hash :initarg :prev-hash)
   (hash :accessor hash :initarg :hash)
   (data :accessor data :initarg :data)
   (flavor :accessor flavor :initarg :flavor)))


(defmethod show-block ((b <block>))
  "create a string representation of a block"
  (labels ((pad (n s) (let ((padding (make-string n :initial-element #\.)))
                        (setf (subseq padding 0 (length s)) s)
                        padding))
           (num-fmt (n) (pad 64 (format nil "~d" n)))
           (str-fmt (s) (pad 64 s))
           (any-fmt (x) (pad 64 (format nil "~a" x))))
    (let ((delimiter (make-string 64 :initial-element #\=)))
      (format nil (concatenate 'string delimiter       "~%"
                               (num-fmt (index b))     "~%"
                               (num-fmt (timestamp b)) "~%"
                               (str-fmt (prev-hash b)) "~%"
                               (str-fmt (hash b))      "~%"
                               (any-fmt (data b))      "~%"
                               (num-fmt (flavor b))    "~%")))))

(defmethod print-block ((b <block>))
  "print a block using the representation from `show-string'"
  (format t (show-block b)))

(defun print-chain (chain)
  "print a chain of blocks"
  (dolist (b chain) (print-block b)))



;; 'flavor' is kind of like a salt value; it's used for proof-of-work
(defun make-header (index prev-hash data timestamp flavor)
  "create a 'header' string that contains important block info"
  (format nil "~d~a~a~d~d" index prev-hash data timestamp flavor))

(defun hash-header (header)
  "calculate the sha256 hash of a block header"
  (let* ((header-bytes (ironclad:ascii-string-to-byte-array header))
         (hashed-header (ironclad:digest-sequence :sha256 header-bytes))
         (hex-hashed-header (ironclad:byte-array-to-hex-string
                             hashed-header)))
    hex-hashed-header))


;; number of leading zeroes used in proof-of-work algorithm.
;; if it's set to 0, that's because doing an actual proof-of-work
;; computation is very intense, which makes testing difficult.
(defparameter *zero-limit* 0)
;; how many flavors to search through per search job
(defparameter *flavor-search-interval* 2)

(defun has-leading-zeroes (n s)
  "determine if string `s' begins with `n' leading zeroes"
  (string= (make-string n :initial-element #\0)
           (subseq s 0 n)))

(defun hash-valid-p (hash)
  "whether or not a hash is valid, as far as proof-of-work is concerned"
  (char> (char hash 0) #\9))

;; returns `nil | (hash . flavor)'
(defun find-header (index prev-hash data timestamp start-flavor)
  "find the flavor that makes the header data hash to have `num-zeroes' zeroes"
  (labels ((find-flavor (flavor)
             (let* ((header (make-header index prev-hash data timestamp flavor))
                    (hash (hash-header header)))
               (unless (>= flavor (+ start-flavor *flavor-search-interval*))
                 (if (hash-valid-p hash)
                     (cons hash flavor)
                     (find-flavor (1+ flavor)))))))
    (find-flavor start-flavor)))





;; note: the genesis block doesn't currently do the whole
;; proof-of-work dance, in order to make starting a chain easier
(defun make-genesis-block (data)
  "make the first block in a blockchain"
  (let* ((time (get-universal-time))
         (header (make-header 0 nil data time 0))
         (hash (hash-header header)))
    (make-instance '<block> :index 0 :data data
                            :prev-hash nil :hash hash
                            :flavor 0 :timestamp time)))


(defun start-mining-next (chain peers)
  "start mining the block after `prev-block'"
  (let ((data (format nil "suya #~d" (random #xff))))
    (try-mine chain (get-universal-time) data peers)))


(defun try-mine (chain time data peers &optional (flavor 0))
  "try to create a new block at the end of the chain"
  (let* ((prev-block (car chain))
         (idx (1+ (index prev-block)))
         (prev-hash (hash prev-block))
         (soln (find-header idx prev-hash data time flavor)))
    (if soln
        (let* ((b (make-instance '<block> :index idx :data data
                                          :prev-hash prev-hash :hash (car soln)
                                          :flavor (cdr soln) :timestamp time))
               (new-chain (cons b chain)))
          (format t "mined a new block, with data ``~a''!~%" data)
          (broadcast-block b peers)
          (start-mining-next new-chain peers)
          new-chain)
        (progn (add-job *jobs* (make-job #'try-mine
                                         chain time data peers
                                         (+ flavor *flavor-search-interval*)))
               nil))))



;; a new block `b' after current end of chain `e' is valid iff:
;; 1. it hashes to the value that it claims to
;; 2. its prev-hash is e's hash
;; 3. its hash value is valid according to the proof-of-work scheme
(defun new-block-valid-p (b chain)
  "see if block `b' is a valid next block in `chain'"
  (let ((b-hash (hash-block b))
        (prev-hash (prev-hash b)))
    (and (string= prev-hash (hash (car chain)))
         (string= (hash b) b-hash)
         (hash-valid-p b-hash))))



(defun alist->block (alist)
  "make a block from a alist (i.e., a deserialized json object)"
  (flet ((lookup (k) (cdr (assoc k alist))))
    (make-instance '<block>
                   :index (lookup :index)
                   :timestamp (lookup :timestamp)
                   :prev-hash (lookup :prev-hash)
                   :hash (lookup :hash)
                   :data (lookup :data)
                   :flavor (lookup :flavor))))

(defun json->block (block-json)
  "convert a block in JSON format to CLOS format"
  (alist->block (cl-json:decode-json-from-string block-json)))

(defmethod block->json ((b <block>))
  "convert a block to a json string"
  (cl-json:encode-json-to-string b))

(defun json->chain (chain-json)
  "convert a chain from JSON format to CLOS format"
  (let ((plists (cl-json:decode-json-from-string chain-json)))
      (mapcar #'alist->block plists)))

(defun load-chain (chain-file)
  "load a blockchain from a json file"
  (with-open-file (s chain-file
                     :direction :input)
    (let ((plists (cl-json:decode-json s)))
      (mapcar #'alist->block plists))))

(defun load-remote-chain (address)
  "load a blockchain from a remote node (server)"
  (let* ((response (drakma:http-request address))
         (chain-json (flexi-streams:octets-to-string response)))
    (json->chain chain-json)))


(defmethod save-chain (chain chain-file)
  "save an entire blockchain as json in `chain-file'"
  (with-open-file (s chain-file
                     :direction :output
                     :if-exists :supersede)
    (cl-json:encode-json chain s)))



(defun sync-chain (chain-file other-chains)
  "update the chain in `chain-file' based on other chains"
  (let* ((my-chain (load-chain chain-file))
         (best (best-chain (cons my-chain other-chains))))
    (save-chain best chain-file)))

(defun best-chain (chains)
  "given multiple (>=1) chains, pick the 'best' one"
  ;; NOTE: 'best' is currently defined to mean 'longest'.
  (reduce #'longer-chain chains))

;; a more realistic implementation would validate each chain
(defun longer-chain (chain-a chain-b)
  "choose the longer chain of `chain-a' and `chain-b'"
  (if (> (length chain-a) (length chain-b))
         chain-a chain-b))


;; NOTE: peers should be full urls, not just the host name.
;; This is to allow for endpoints besides /location, although
;; this extra bit of flexibility is ultimately of very little
;; importance for such a small prototype.
(defmethod broadcast-block ((b <block>) peers)
  "broadcast a new block to peer mining nodes"
  (dolist (p peers)
    ;; if a peer is inaccessible, it's more preferable to
    ;; ignore it rather than crash the whole program.  logging
    ;; the error would be nice to have a some point, too.
    (ignore-errors
     (drakma:http-request
      p :method :post
        :parameters (list (cons "block" (block->json b)))))))


;;;;;;;;;;;;;;;; web server

(defun run-server (chain-file port)
  "provide `chain-file' at 127.0.0.1:`port'; returns acceptor"
  ;; acceptor gets returned so that you can stop it at some point
  (let ((acceptor (make-instance 'hunchentoot:easy-acceptor :port port
                                 :document-root (merge-pathnames #p"www/"))))
    (hunchentoot:define-easy-handler
        (get-chain :uri "/") ()
      (setf (hunchentoot:content-type*) "application/json")
      (uiop:read-file-string chain-file))

    (hunchentoot:define-easy-handler
        (receive-new-block :uri "/submit") (block)
      (setf (hunchentoot:content-type*) "application/json")
      (let ((b (json->block block)))
        (format t "[~d] new block submitted:~%" port)
        (print-block b)))
    
    (hunchentoot:start acceptor)
    acceptor))
;; (defparameter *server* (run-server (merge-pathnames #p"chain.json") 5555))


;;;;;;;;;;;;;;;; putting everything together

(defparameter *genesis* (make-genesis-block "soggy bottom block"))

;; from rosetta-code
(defmacro set-signal-handler (signo &body body)
  (let ((handler (gensym "HANDLER")))
    `(progn
       (cffi:defcallback ,handler :void ((signo :int))
         (declare (ignore signo))
         ,@body)
       (cffi:foreign-funcall "signal" :int ,signo :pointer (cffi:callback ,handler)))))

(set-signal-handler
 2 (format t "bye!~%")
 (sb-ext:quit))

(defun main (chain-file port peer)
  (format t "starting server on port ~d...~%" port)
  (run-server chain-file port)
  (format t "mining chain at ~a...~%" chain-file)
  (start-mining-next (list *genesis*) (list peer))
  (loop (sleep 2)
        (do-next-job *jobs*)))
