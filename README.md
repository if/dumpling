# dumpling

Experiments with blockchain and coin stuff.  Most of the
initial code is adapted from this [tutorial](https://bigishdata.com/2017/10/17/write-your-own-blockchain-part-1-creating-storing-syncing-displaying-mining-and-proving-work/).

![dumpling logo](logo.png)

## What you can do with dumpling, as of <present day, present time>
You can use `./run-node CHAIN-FILE PORT PEER` to run a mining node on
`http://localhost:PORT`, saving a json representation of the chain in `CHAIN-FILE`
and communicating with a single peer node, at the address `PEER`.

Right now, each node mines successive blocks, reports new blocks to its peer,
and listens for new blocks from its peer.  There are a few basic things that still
need to be done:
- actively storing new blocks in `CHAIN-FILE`
- validating blocks from peers
- synchronizing with peers (probably the hardest of these three)

A lot of those three things are partially implemented already, so feel free
to take a look at `dumpling.lisp` if you're interested.

Finally, there are a few things I want to experiment with, once those aforementioned
'basic things' are implemented:
- use https
- try out alternatives to proof-of-work
- improve consensus and synchronisation mechanisms (???)
- use dumpling for (non-serious) NFTs, monetary transactions, or something else


## License
GPLv3
